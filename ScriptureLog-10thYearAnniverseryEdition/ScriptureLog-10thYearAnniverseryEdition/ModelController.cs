﻿using System.Collections.Generic;

using Foundation;
using UIKit;

using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using static System.Net.Mime.MediaTypeNames;

namespace ScriptureLog10thYearAnniverseryEdition
{
    public class ModelController : UIPageViewControllerDataSource
    {
        // readonly List<string> pageData;
        List<string> pageData;

        // string directory = @".\Resources\";
        //List<Image> stories;

        public ModelController()
        {
            // var formatter = new NSDateFormatter();
            // pageData = new List<string>(formatter.MonthSymbols);
            var numPages = 50;
            pageData = new List<string>();
            for (int i = 1; i < numPages; i++)
            {
                pageData.Add(i.ToString());
            }

            //List<Image> stories = new List<Image>();
            //foreach (string myFile in Directory.GetFiles("Resources", "*.png", SearchOption.AllDirectories))
            //{
            //    //Image story = new Image { Source = myFile };

            //}

            //stories = new List<UIImage>();
            //foreach (string myFile in Directory.GetFiles(directory, "*.png", SearchOption.AllDirectories))
            //{
            //    UIImage story = new UIImage();

            //    BitmapImage source = new BitmapImage();
            //    source.BeginInit();
            //    source.UriSource = new Uri(myFile, UriKind.Relative);
            //    source.EndInit();
            //    story.Source = source;

            //    stories.Add(story);
            //}
        }

        public DataViewController GetViewController(int index, UIStoryboard storyboard)
        {
            if (index >= pageData.Count)
                return null;

            // Create a new view controller and pass suitable data.
            var dataViewController = (DataViewController)storyboard.InstantiateViewController("DataViewController");
            dataViewController.DataObject = pageData[index];

            return dataViewController;
        }

        public int IndexOf(DataViewController viewController)
        {
            //Globals.pageNum = Int32.Parse(viewController.DataObject);
            Console.WriteLine(Globals.pageNum);
            return pageData.IndexOf(viewController.DataObject);
        }

        #region Page View Controller Data Source

        public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            int index = IndexOf((DataViewController)referenceViewController);

            if (index == -1 || index == pageData.Count - 1)
                return null;

            return GetViewController(index + 1, referenceViewController.Storyboard);
        }

        public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            int index = IndexOf((DataViewController)referenceViewController);

            if (index == -1 || index == 0)
                return null;

            return GetViewController(index - 1, referenceViewController.Storyboard);
        }

        #endregion
    }
}
