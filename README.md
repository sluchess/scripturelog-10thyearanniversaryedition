# ScriptureLog-10thYearAnniversaryEdition

When I was 12 years old, I created a Scripture log comic book for my religion class in middle school. My mother loved the art and work I put into it but the book ended up being lost in our basement for several years. It was found recently, about ten years later. This project is the original scripture log turned into an IOS application and given to my mother as a Christmas gift.
